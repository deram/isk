# frozen_string_literal: true

class GenerateSlidesJob < ApplicationJob
  queue_as :schedule

  def perform(record)
    record.generate_slides
  end
end
