#
#  ticket_tab_update.js.coffee
#  isk
#
#  Created by Vesa-Pekka Palmu on 2019-07-24.
#  Copyright 2019 Vesa-Pekka Palmu. All rights reserved.
#

$ ->
	check_tickets = ->
		console.log 'Checking tickets'
		$.ajax({
			type: "GET",
			url: "/tickets",
			dataType: 'script'
		})

	# Check tickets every 30 seconds, keep repeating
	timer = $.timer( check_tickets, 30 * 1000, true)

	# Also run the check at page load
	check_tickets()