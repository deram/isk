@isk or= {}

main_canvas = undefined
clock_div = undefined

elem = undefined
ctx = undefined
time = undefined
shown = true
old_t = ""

t_pos = [1630, 40]
t_size = 50

locale = undefined # undefined means browsers locale
datestyle = {weekday: 'long'}
timestyle = {hour: '2-digit', minute: '2-digit', second: '2-digit'}

init = ->
  main_canvas = document.querySelector('#ISKDPY div#canvas canvas')
  clock_div = document.querySelector('#ISKDPY div#clock')
  time = new Date()

  elem=document.createElement('canvas')
  ctx=elem.getContext('2d')
  elem.width=1920
  elem.height=128

  ctx.font = t_size + "px 'CustomFont'"
  ctx.fillStyle = '#FFF'
  ctx.strokeStyle = '#000'
  ctx.lineWidth = 3
  ctx.miterLimit = 2

  clock_div.appendChild elem
  document.defaultView.addEventListener('resize', handleWindowSizeChange)

set_time_locale = (l) ->
  locale=l

set_time_size = (s) ->
  t_size=s
  ctx.font = t_size + "px 'CustomFont'"

set_time_pos = (x,y) ->
  t_pos = [x,y]

show = ->
  elem.classList.remove 'hidden'
  shown=true

hide = ->
  elem.classList.add 'hidden'
  shown='hiding'
  setTimeout ->
    shown=false if shown == 'hiding'
  , 2000
  false

drawStrokedText = (t, pos...) ->
  ctx.fillText(t, pos...)
  ctx.strokeText(t, pos...)

set_current_time = () ->
  if shown
    t=Math.floor(Date.now() / 1000)
    if t != old_t
      old_t = t
      time.setTime(Date.now())

      ctx.clearRect(0, 0, elem.width, elem.height)
      drawStrokedText(time.toLocaleString(locale, datestyle), t_pos[0], t_pos[1])
      drawStrokedText(time.toLocaleString(locale, timestyle), t_pos[0], t_pos[1] + t_size)

handleWindowSizeChange = (event...) ->
  bottom_y = window.innerHeight - main_canvas.height
  clock_div.style['bottom'] = "#{bottom_y}px"
  clock_div.style['width'] = "#{main_canvas.width}px"
  true

run = (t) ->
  requestAnimationFrame run
  set_current_time()

setTimeout ->
  # Initialize module.
  init()
  # Delayed startup after every other modules are initialized.
  setTimeout ->
    handleWindowSizeChange()
    run()

#EXPORTS:
isk.clock =
  show: show
  hide: hide
  size: set_time_size
  pos: set_time_pos
  locale: set_time_locale
