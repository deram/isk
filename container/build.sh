#!/bin/bash
docker build -t registry.gitlab.com/iskcrew/isk/base base/
docker build -t registry.gitlab.com/iskcrew/isk/app -f app/Dockerfile ../
docker build -t registry.gitlab.com/iskcrew/isk/postgres postgres/
docker build -t registry.gitlab.com/iskcrew/isk/nginx nginx/
