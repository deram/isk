# frozen_string_literal: true

require "test_helper"

class ImagesControllerTest < ActionController::TestCase

  def setup
    @adminsession = { user_id: users(:admin).id, username: users(:admin).username }
    @no_roles_session = { user_id: users(:no_roles).id, username: users(:no_roles).username}

    Slide.send(:remove_const, :FilePath)
    Slide.const_set(:FilePath, Rails.root.join("tmp", "test"))

    init_slide_files(slides(:simple))
    s = Slide.find(slides(:simple).id)
    s.images_updated_at = Time.now
    s.save!
  end

  def teardown
    clear_slide_files(slides(:simple))
  end

  test "get thumbnail" do
    get :show, params: {slide_id: slides(:simple).id, size: "thumb"}, session: @no_roles_session
    assert_response :success
  end

  test "get preview" do
    get :show, params: {slide_id: slides(:simple).id, size: "preview"}, session: @no_roles_session
    assert_response :success
  end

  test "get full image" do
    get :show, params: {slide_id: slides(:simple).id}, session: @no_roles_session
    assert_response :success
  end

  test "get transparent image" do
    get :show, params: {slide_id: slides(:simple).id, size: "transparent"}, session: @no_roles_session
    assert_response :success
  end

  test "no images" do
    get :show, params: {slide_id: slides(:inkscape).id, size: "transparent"}, session: @no_roles_session
    assert_response :not_found
  end
end
