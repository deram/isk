# frozen_string_literal: true

require "test_helper"

class TicketsControllerTest < ActionController::TestCase
  def setup
    @adminsession = { user_id: users(:admin).id, username: users(:admin).username }
    @no_roles_session = { user_id: users(:no_roles).id, username: users(:no_roles).username}
    @new_ticket_data = {
                         ticket: {
                           name: "New test ticket",
                           description: "Ticket test\ndescription\nhere",
                           about_type: "Slide",
                           about_id: slides(:no_clock).id
                         }
                       }
  end

  test "get index" do
    get :index, session: @adminsession
    assert_response :success, "Failed to get tickets list as admin"
  end

  test "get ticket info" do
    [
      :slide_ticket, :master_group_ticket, :presentation_ticket, :closed_ticket
    ].each do |t|
      get :show, params: { id: tickets(t) }, session: @adminsession
      assert_response :success, "Failed to get info for ticket #{t} as admin"
    end
  end

  test "create new ticket" do
    get :new, session: @adminsession
    assert_response :success

    assert_difference("Ticket.count", 1) do
      post :create, params: @new_ticket_data, session: @adminsession
    end

    assert_redirected_to ticket_path(assigns(:ticket)), "Didn't redirect to ticket page"
    assert_equal @new_ticket_data[:ticket][:name], assigns(:ticket).name, "Ticket didn't have correct name"
    assert assigns(:ticket).about.present?
    assert assigns(:ticket).about.is_a? Slide
    assert assigns(:ticket).about_id == @new_ticket_data[:ticket][:about_id]
  end

  test "try to create invalid ticket" do
    assert_no_difference("Ticket.count") do
      post :create, params: { ticket: { name: "Invalid" } }, session: @adminsession
      assert_template :new
    end
  end

  test "update ticket" do
    get :edit, params: { id: tickets(:presentation_ticket).id }, session: @adminsession
    assert_response :success

    put :update, params: {id: tickets(:presentation_ticket).id, ticket: { description: "Updated" }},
      session: @adminsession
    assert_redirected_to ticket_path(assigns(:ticket))
    assert_equal "Updated", assigns(:ticket).description
    assert_equal Ticket::StatusOpen, assigns(:ticket).status
  end

  test "try to update invalid ticket" do
    put :update, params: { id: tickets(:open_ticket), ticket: { name: "" } }, session: @adminsession
    assert_template :edit
  end

  test "delete ticket" do
    assert_difference("Ticket.count", -1) do
      delete :destroy, params: { id: tickets(:slide_ticket).id }, session: @adminsession
    end

    assert_redirected_to tickets_path
  end

  test "delete ticket without admin rights" do
    assert_no_difference("Ticket.count") do
      delete :destroy, params: { id: tickets(:slide_ticket).id }, session: @no_roles_session
    end

    assert_response :forbidden
  end
end
