Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  config.file_watcher = ActiveSupport::FileUpdateChecker

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }

    # Memcached using dalli_store
    config.cache_store = :dalli_store, nil,
                         { namespace: "ISK", expires_in: 15.minutes, compress: true }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Don't log served assets
  config.assets.logger = false
  config.serve_static_files = true

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  unless ENV['RAILS_DISABLE_FILE_WATCHER'].present?
    # Use an evented file watcher to asynchronously detect changes in source code,
    # routes, locales, etc. This feature depends on the listen gem.
    config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  end

  # Websockets needs this, otherwise the websocket connection will
  # lock the server up completely.
  config.middleware.delete Rack::Lock

  # Rewrite rules so the simple editor view finds it's backgrounds
  config.middleware.insert(0, Rack::Rewrite) do
    rewrite %r{/backgrounds/(.*)}, "/backgrounds/$1"
  end
end
